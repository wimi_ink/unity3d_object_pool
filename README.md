# Unity3D对象池

#### 介绍
Unity3D简单的对象池模型，可控制多个对象池

#### 目录介绍
UIbtn 控制UGUI 用于点击生成
CubeTest 简单的生成的物体测试（移动到对象池）
PoolControl 单个对象池 生成存放（必要）
ObjectPoolControl 控制多个对象池，对单个对象池操作（必要）

#### 使用说明

1.  创建一个空物体作为总对象池控制  脚本（ObjectPoolControl）
2.  创建一个空物体作为单对象池  脚本（PoolControl）
3.  需要创建的物体放入单对象池，一个创建一个单对象池
4.  全部值请赋值完成

#### 参与贡献

1.  WeiMiao 第一次提交
![输入图片说明](https://images.gitee.com/uploads/images/2020/0802/142915_19a91ddb_1435149.gif "1.gif")
