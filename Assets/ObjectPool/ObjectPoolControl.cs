﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * 此脚本是做为一个总控制
 * 控制多个对象池
 */
namespace WeiMiao
{
    public class ObjectPoolControl : MonoBehaviour
    {
        [Tooltip("所有的对象池")]
        public GameObject[] pool;

        /// <summary>
        /// 取出对象
        /// </summary>
        /// <param name="manypool">操作第N个对象池里的对象</param>
        /// <returns>取出的对象</returns>
        internal GameObject OperationOut(int manypool)
        {
            //执行对象池里的取出操作
            return pool[manypool].GetComponent<PoolControl>().OutObject();
        }

        /// <summary>
        /// 存放对象
        /// </summary>
        /// <param name="manypool">操作第N个对象池里的对象</param>
        /// <param name="game">存放的对象</param>
        internal void OperationAdd(int manypool, GameObject game)
        {
            //执行对象池里的存放操作
            pool[manypool].GetComponent<PoolControl>().AddObjects(game);
        }
    }

}
