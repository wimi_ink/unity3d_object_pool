﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 此脚本是控制单个对象池
 * 存放和取出操作
 */

namespace WeiMiao
{
    internal class PoolControl : MonoBehaviour
    {
        [Tooltip("基本对象")]
        public GameObject primary_object;

        [Tooltip("生成的对象")]
        List<GameObject> objects = new List<GameObject>();

        /// <summary>
        /// 从对象池取出
        /// </summary>
        /// <returns>取出的对象</returns>
        internal GameObject OutObject()
        {
            //定义一个游戏对象
            GameObject game;
            //判断对象池是否为空
            if (objects.Count == 0)
            {
                //如果为空
                //从定义的基本对象生成
                game = Instantiate(primary_object);
            }
            else
            {
                //如果不为空
                //计算，最后一个数
                int number = objects.Count - 1;
                //获取对象
                game = objects[number];
                //删除列表里取出的对象
                objects.RemoveAt(number);
            }
            //启用对象
            game.SetActive(true);
            //返回对象
            return game;
        }

        /// <summary>
        /// 加入对象池
        /// </summary>
        /// <param name="game">要加入的对象</param>
        internal void AddObjects(GameObject game)
        {
            game.SetActive(false);
            //添加至对象列表
            objects.Add(game);
            //移动位置
            game.transform.parent = this.transform;
        }
    }
}
