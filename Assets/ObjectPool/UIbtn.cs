﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeiMiao;

public class UIbtn : MonoBehaviour
{
    [Tooltip("对象池")]
    public GameObject ObjectPool;
    [Tooltip("（生成/取出）的对象存放处")]
    public GameObject Generation;

    /// <summary>
    /// （生成/取出）后对象池的操作
    /// </summary>
    /// <param name="manypool">第几个对象池</param>
    public void Generate(int manypool)
    {
        //取出对象
        GameObject game = ObjectPool.GetComponent<ObjectPoolControl>().OperationOut(manypool);
        //给对象添加属性
        //移动到存放处
        game.transform.parent = Generation.transform;
        //计算时间
        game.GetComponent<CubeTest>().time = Time.time;
        //第几个对象池
        game.GetComponent<CubeTest>().manypool = manypool;
        //传入对象池
        game.GetComponent<CubeTest>().ObjectPool = ObjectPool;

    }
}
