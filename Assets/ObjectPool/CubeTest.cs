﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WeiMiao;

public class CubeTest : MonoBehaviour
{
    [Tooltip("对象池")]
    public GameObject ObjectPool;
    [Tooltip("加入第几个对象池")]
    public int manypool;
    [Tooltip("生成时间")]
    public float time;
    [Tooltip("获取存活时间")]
    public GameObject UiInputField;


    // Update is called once per frame
    void Update()
    {
        //计算以存活的时间
        if(Time.time - time > Convert.ToInt32(UiInputField.GetComponent<Text>().text))
        {
            //移入对象池
            ObjectPool.GetComponent<ObjectPoolControl>().OperationAdd(manypool, this.gameObject);
        }
    }
}
